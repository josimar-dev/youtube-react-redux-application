import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list';
import VideoListItem from './components/video_list_item';
import VideoDetail from './components/video_details';
import _ from 'lodash';

const API_KEY = 'AIzaSyCeWw86yu9IRGJCHZHLbd6IgDNnVYWmqi0';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { videos: [] };
    this.videoSearch("funfunfunction");
  }

  videoSearch = (term) => {
    YTSearch({ key: API_KEY, term: term }, videos => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
       });
    });
  }

  render() {
    /* "debounce" function is gonna make sure that the callback get fired after 3 miliseconds */
    const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);
    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          videos={this.state.videos}
          onVideoSelect={selectedVideo => this.setState({selectedVideo})}
        />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector('.container'));
